<?php
namespace Mindaugas\StaffRegister\Models;

use PDO;

class Database
{
    private $host = "localhost";
    private $db_name = "cli";
    private $username = "cli";
    private $password = "cli";
    public $conn;

    protected static $instance = null;

    public static function getInstance()
    {
        if (!isset(static::$instance)) {
            static::$instance = new Database;
        }
        return static::$instance;
    }

    protected function __construct()
    {
        $this->conn = null;
        try {
            $this->conn = new PDO("mysql:host=".$this->host.";dbname=".$this->db_name, $this->username, $this->password);
        }
        catch(PDOException $exception) {
            echo "Database connection error: " . $exception->getMessage();
        }
        return $this;
    }
}
