<?php
namespace Mindaugas\StaffRegister\Models;


use PDO;
use Mindaugas\StaffRegister\Models\Database;
use Mindaugas\StaffRegister\Models\StaffMember;

class StaffRepository
{
	private $conn;
	private $table_name = "staff";

	public function __construct()
	{
		$this->conn = Database::getInstance()->conn;
	}

	public function loadById($id){
		$query = "SELECT * FROM ".$this->table_name." WHERE id = :id";
		$q = $this->conn->prepare($query);
		$q->bindParam(':id', $id);
		$q->execute();
		$data = $q->fetch(PDO::FETCH_ASSOC);
		$member = new StaffMember($data);
		return $member;
	}

	public function loadAll($from, $pageSize, $searchTerm=null)
	{
		$query = "SELECT id, firstname, lastname, email, phonenumber1, phonenumber2, comment FROM ".$this->table_name;
		if ($searchTerm) {
			$query .=" WHERE firstname LIKE '%{$searchTerm}%' OR lastname LIKE '%{$searchTerm}%'";
		}
		$query .=" ORDER BY lastname ASC LIMIT {$from}, {$pageSize}";
		$q = $this->conn->prepare($query);
		$q->execute();

		$result=[];
		while ($row = $q->fetch(PDO::FETCH_ASSOC)) {
			$member = new StaffMember($row);
			$result[] = $member;
		}

		return $result;
	}

	public function countAll($searchTerm=null)
	{
		$query = "SELECT COUNT(*) as total_rows FROM ".$this->table_name;
		if ($searchTerm) {
			$query .=" WHERE firstname LIKE '%{$searchTerm}%' OR lastname LIKE '%{$searchTerm}%'";
		}
		$q = $this->conn->prepare($query);
		$q->execute();
		$row = $q->fetch(PDO::FETCH_ASSOC);
		return $row['total_rows'];
	}

	public function deleteById($id)
	{
		$query = "DELETE FROM ".$this->table_name." WHERE id = ?";
		$q = $this->conn->prepare($query);
		$q->bindParam(1, $id);
		if ($result = $q->execute()) {
			return true;
		}
		else {
			return false;
		}
	}

}
