<?php
namespace Mindaugas\StaffRegister\Models;

use PDO;
use Mindaugas\StaffRegister\Models\Database;

class StaffMember
{
    private $conn;
    private $table_name = "staff";
 
    private $id=null;
    public function getId()
    {
        return $this->id;
    }

    private $firstname="";
    public function getFirstname()
    {
        return $this->firstname;
        return $this;
    }
    public function setFirstname($firstname)
    {
        $this->firstname = $firstname;
    }

    private $lastname="";
    public function getLastname()
    {
        return $this->lastname;
    }
    public function setLastname($lastname)
    {
        $this->lastname = $lastname;
        return $this;
    }

    private $email="";
    public function getEmail()
    {
        return $this->email;

    }
    public function setEmail($email)
    {
        $this->email = $email;
        return $this;
    }

    private $phonenumber1="";
    public function getPhonenumber1()
    {
        return $this->phonenumber1;

    }
    public function setPhonenumber1($phonenumber1)
    {
        $this->phonenumber1 = $phonenumber1;
        return $this;
    }

    private $phonenumber2="";
    public function getPhonenumber2()
    {
        return $this->phonenumber2;

    }
    public function setPhonenumber2($phonenumber2)
    {
        $this->phonenumber2 = $phonenumber2;
        return $this;
    }

    private $comment="";
    public function getComment()
    {
        return $this->comment;

    }
    public function setComment($comment)
    {
        $this->comment = $comment;
        return $this;
    }
 
    public function __construct($arr=null)
    {
        if ($arr!==null) {
            if(isset($arr["id"]))$this->id=$arr["id"];
            if(isset($arr["firstname"]))$this->firstname=$arr["firstname"];
            if(isset($arr["lastname"]))$this->lastname=$arr["lastname"];
            if(isset($arr["email"]))$this->email=$arr["email"];
            if(isset($arr["phonenumber1"]))$this->phonenumber1=$arr["phonenumber1"];
            if(isset($arr["phonenumber2"]))$this->phonenumber2=$arr["phonenumber2"];
            if(isset($arr["comment"]))$this->comment=$arr["comment"];
        }
        $this->conn = Database::getInstance()->conn;
    }

    function save()
    {
        if ($this->id) {
            return $this->update();
        }
        $query = "INSERT INTO ".$this->table_name." SET firstname=:firstname, lastname=:lastname, email=:email, phonenumber1=:phonenumber1, phonenumber2=:phonenumber2, comment=:comment";
        $q = $this->conn->prepare($query);
        $q->bindParam(':firstname', $this->firstname);
        $q->bindParam(':lastname', $this->lastname);
        $q->bindParam(':email', $this->email);
        $q->bindParam(':phonenumber1', $this->phonenumber1);
        $q->bindParam(':phonenumber2', $this->phonenumber2);
        $q->bindParam(':comment', $this->comment);
        if ($q->execute()) {
            return true;
        }
        else {
            return false;
        }
    }

    function update()
    {
        if (!$this->id) {
            return $this->save();
        }
        $query = "UPDATE ".$this->table_name." SET firstname=:firstname, lastname=:lastname, email=:email, phonenumber1=:phonenumber1, phonenumber2=:phonenumber2, comment=:comment WHERE id = :id";
        $q = $this->conn->prepare($query);
        $q->bindParam(':id', $this->id);
        $q->bindParam(':firstname', $this->firstname);
        $q->bindParam(':lastname', $this->lastname);
        $q->bindParam(':email', $this->email);
        $q->bindParam(':phonenumber1', $this->phonenumber1);
        $q->bindParam(':phonenumber2', $this->phonenumber2);
        $q->bindParam(':comment', $this->comment);
        if ($q->execute()) {
            return true;
        }
        else{
            return false;
        }
    }

    function delete()
    {
        if (!$this->id) {
            return false;
        }
        $query = "DELETE FROM " . $this->table_name . " WHERE id = ?";
        $q = $this->conn->prepare($query);
        $q->bindParam(1, $this->id);
        if ($result = $q->execute()) {
            return true;
        }
        else {
            return false;
        }
    }
}
