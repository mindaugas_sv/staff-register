<?php
namespace Mindaugas\StaffRegister\Core;

class Console{
    static public function read($brbefore=0,$brafter=0)
    {
        for ($i=0; $i < $brbefore; $i++) {
            echo PHP_EOL;
        }
        $handle = fopen('php://stdin', 'r');
        $input = fgets($handle);
        fclose($handle);
        for ($i=0; $i < $brafter; $i++) {
            echo PHP_EOL;
        }
        //$input = trim($input);
        $input = rtrim($input, "\n\r");
        return $input;
    }
    static public function print($brbefore=0,$brafter=0,$text)
    {
        for ($i=0; $i < $brbefore; $i++) {
            echo PHP_EOL;
        }
        echo $text;
        for ($i=0; $i < $brafter; $i++) {
            echo PHP_EOL;
        }
        return true;
    }

    static public function printCell($brbefore=0,$brafter=0,$text,$length=13,$position='left',$delimiter="|")
    {
        for ($i=0; $i < $brbefore; $i++) {
            echo PHP_EOL;
        }
        $length=$length-strlen($delimiter);
        switch ($position) {
            case 'right':
                $position=STR_PAD_LEFT;
                break;
            case 'center':
                $position=STR_PAD_BOTH;
                break;
            case 'left':
            default:
                $position=STR_PAD_RIGHT;
        }
        if (strlen($text)>$length) {
            $text=substr($text,0,$length-2).'..';
        }
        $text = str_pad($text,$length,' ',$position).$delimiter;
        echo $text;
        for ($i=0; $i < $brafter; $i++) {
            echo PHP_EOL;
        }
        return true;
    }
}
