<?php
namespace Mindaugas\StaffRegister\Core;

use Mindaugas\StaffRegister\Functions;

class FunctionLoader
{
	public static function load($function,$method = "Index")
	{
		$function=ucfirst($function);
		$method=ucfirst($method);
		$class = 'Mindaugas\\StaffRegister\\Functions\\Help\\Index';
		if (file_exists(__DIR__.'/../Functions/'.$function.'/'.$method.'.php')) {
			$checkClass = 'Mindaugas\\StaffRegister\\Functions\\'.$function.'\\'.$method;
			if (class_exists($checkClass)) {
				$class = $checkClass;
			}
		}
		return new $class();
	}

	public static function loadAllFunctionClasses(){
		$dir = __DIR__.'/../Functions/';
		$dh = opendir($dir);
		$dir_list = array($dir);
		while (false !== ($filename = readdir($dh))) {
			if($filename != "." && $filename != ".." && is_dir($dir.$filename)){
				array_push($dir_list, $dir.$filename."/");
			}
		}
		foreach ($dir_list as $dir) {
			foreach (glob($dir."*.php") as $filename){
				require_once $filename;
			}
		}
	}
}
