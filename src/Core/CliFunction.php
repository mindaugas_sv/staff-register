<?php
namespace Mindaugas\StaffRegister\Core;

class CliFunction{
	public static function function($function,$method = "Index") {
		$function = ucfirst($function);
		$method = ucfirst($method);
		$class = 'Mindaugas\\StaffRegister\\Functions\\Help\\Index';
		if (file_exists(__DIR__.'/../Functions/'.$function.'/'.$method.'.php')) {
			$checkClass = 'Mindaugas\\StaffRegister\\Functions\\'.$function.'\\'.$method;
			if (class_exists($checkClass)) {
				$class = $checkClass;
			}
		}
		return new $class();
	}

	public function run()
	{
		return 'Function doesn\'t work';
	}

	public function getDescription()
	{
		return 'description not available';
	}

	public function model($model)
	{
		$class=null;
		if (file_exists(__DIR__.'/../Models/'.$model.'.php')) {
			$checkClass = 'Mindaugas\\StaffRegister\\Models\\'.$model;
			if (class_exists($checkClass)) {
				$class = $checkClass;
			}
		}
		if ($class) {
			return new $class();
		}
		else {
			return false;
		}
	}
}
