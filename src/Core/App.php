<?php
namespace Mindaugas\StaffRegister\Core;

use Mindaugas\StaffRegister\Core\CliFunction;
use Mindaugas\StaffRegister\Core\Console;
use Mindaugas\StaffRegister\Core\FunctionLoader;

class App{
	protected $defaultFunction = 'help';
	protected $defaultMethod = 'index';

	protected $params = [];

	public function __construct(){
		$hello = FunctionLoader::load('help','hello');
		$hello->run();
		$this->readCommandInput();
	}

	public function readCommandInput(){
		Console::print(1,0,'Type your command or "help" for commands list: ');
		$input = Console::read(0,1);
		$inputArray = explode(":", $input);
		$functionName = $this->defaultFunction;
		$methodName = $this->defaultMethod;
		if (isset($inputArray[0])) {
			$functionName = $inputArray[0];
		}
		if (isset($inputArray[1])) {
			$methodName = $inputArray[1];
		}
		$function = CliFunction::function($functionName,$methodName);
		$function->run();
		$this->readCommandInput();
	}
}
