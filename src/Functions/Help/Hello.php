<?php
namespace Mindaugas\StaffRegister\Functions\Help;

use Mindaugas\StaffRegister\Core\CliFunction;
use Mindaugas\StaffRegister\Core\Console;

class Hello extends CliFunction
{
	public function run()
	{
		Console::print(0,1,'Hello and Wellcome to Staff Register [beta].');
		return true;
	}

	public function getDescription()
	{
		return 'Greets you when you open the program.';
	}
}
