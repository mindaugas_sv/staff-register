<?php
namespace Mindaugas\StaffRegister\Functions\Help;

use Mindaugas\StaffRegister\Core\CliFunction;
use Mindaugas\StaffRegister\Core\FunctionLoader;
use Mindaugas\StaffRegister\Core\Console;

class Index extends CliFunction
{
	public function run()
	{
		FunctionLoader::loadAllFunctionClasses();
		$res = get_declared_classes();
		$functionsNamespace = 'Mindaugas\\StaffRegister\\Functions\\';
		$functionsNamespaceLength = strlen($functionsNamespace);
		foreach ($res as $className) {
			if (strpos($className,$functionsNamespace) === 0) {
				$cliFunctionName = substr($className, $functionsNamespaceLength);
				$cliFunctionName = str_replace('\\',':',$cliFunctionName);
				$cliFunctionName = str_replace(':Index','',$cliFunctionName);
				$cliFunctionName = strtolower($cliFunctionName);
				$class = new $className();
				Console::print(0,1,$cliFunctionName.' ['.$class->getDescription().']');
			}
		}
		return;
	}

	public function getDescription(){
		return 'Shows list of awailable commands.';
	}
}
