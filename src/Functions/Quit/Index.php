<?php
namespace Mindaugas\StaffRegister\Functions\Quit;

use Mindaugas\StaffRegister\Core\CliFunction;
use Mindaugas\StaffRegister\Core\Console;

class Index extends CliFunction
{
	public function run()
	{
		Console::print(0,1,'Thank you for using Staff Register.');
		exit;
	}

	public function getDescription()
	{
		return 'Exits program.';
	}
}
