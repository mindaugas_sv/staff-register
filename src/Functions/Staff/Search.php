<?php
namespace Mindaugas\StaffRegister\Functions\Staff;

use Mindaugas\StaffRegister\Core\CliFunction;
use Mindaugas\StaffRegister\Functions\Staff\Index;
use Mindaugas\StaffRegister\Core\Console;

class Search extends CliFunction {
	public function run(){
		Console::print(0,0,'Search for staff: ');
		$searchTerm = Console::read(0,1);
		if($searchTerm){
			$list = new Index;
			$list->setSearchTerm($searchTerm);
			$list->run();
		}
		return;
	}

	public function getDescription(){
		return 'Opens search dialog for finding staff member.';
	}
}
