<?php
namespace Mindaugas\StaffRegister\Functions\Staff;

use Mindaugas\StaffRegister\Core\CliFunction;
use Mindaugas\StaffRegister\Core\Console;

class Import extends CliFunction {
	public function run(){
		Console::print(0,1,'You are now in import wizard');
		Console::print(0,0,'File location [relative to program path] (leave empty to exit): ');
		$input=Console::read(0,0);

		if(!$input){
			return;
		}

		$file=realpath($input);
		if(!file_exists($file)){
			Console::print(0,0,'File does not exist. ');
			return $this->run();
		}

		Console::print(0,1,'Would you like to import automaticaly, or review proccess');
		Console::print(0,1,'(You will be able to choose auto option after every imported staff member)');
		Console::print(0,1,'Write "a" to import automaticaly;');
		Console::print(0,1,'Write "m" to import manualy;');
		Console::print(0,1,'OR press [enter] to exit');
		

		$runAutomaticaly=false;

		$input=Console::read(0,0);
		$firstLetter = strtolower($input);
		if(strlen($firstLetter) > 0)$firstLetter = $firstLetter[0];
		switch($firstLetter){
			case '':
				return true;
			case 'a':
				$runAutomaticaly = true;
				break;
			case 'm':
				$runAutomaticaly = false;
		}

		$row = 1;
		if (($handle = fopen($file, "r")) !== FALSE) {
			while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
				
				$newStaffMember = $this->model('StaffMember');
				if(isset($data[0]))$newStaffMember->setFirstname($data[0]);
				if(isset($data[1]))$newStaffMember->setLastname($data[1]);
				if(isset($data[2]))$newStaffMember->setEmail($data[2]);
				if(isset($data[3]))$newStaffMember->setPhonenumber1($data[3]);
				if(isset($data[4]))$newStaffMember->setPhonenumber2($data[4]);
				if(isset($data[5]))$newStaffMember->setComment($data[5]);
				

				$import = false;
				if ($runAutomaticaly!==true) {

					Console::print(1,1,'NEW STAFF MEMBER');

					Console::print(0,1,'FIRST NAME: '.$newStaffMember->getFirstname().'');
					Console::print(0,1,'LAST NAME: '.$newStaffMember->getLastname().' ');
					Console::print(0,1,'EMAIL: '.$newStaffMember->getEmail().' ');
					Console::print(0,1,'PHONE NUMBER: '.$newStaffMember->getPhonenumber1().' ');
					Console::print(0,1,'PHONE NUMBER2: '.$newStaffMember->getPhonenumber2().' ');
					Console::print(0,1,'Comment: '.$newStaffMember->getComment().' ');

					Console::print(0,1,'[enter] to import, [s] to skip, [a] to import everything automaticaly, [e] to exit');
					$input=Console::read(0,0);
					$firstLetter = strtolower($input);
					if(strlen($firstLetter)>0)$firstLetter = $firstLetter[0];
					switch($firstLetter){
						case '':
							$import = true;
							break;
						case 's':
							break;
						case 'a':
							$runAutomaticaly=true;
							break;
						case 'e':
							return true;
					}
				}

				if ($import==true || $runAutomaticaly==true) {
					if($newStaffMember->save()){
							Console::print(0,1,'Staff member have been saved successfully');
						}
						else{
							Console::print(0,1,'Staff member have not been saved');
					}
				}
				else{
					Console::print(0,1,'skipping');
				}
			}
			fclose($handle);
		}
		return true;
	}

	public function getDescription(){
		return 'Imports staff members from csv file.';
	}
}
