<?php
namespace Mindaugas\StaffRegister\Functions\Staff;

use Mindaugas\StaffRegister\Core\CliFunction;
use Mindaugas\StaffRegister\Core\Console;

class Index extends CliFunction {

	private $page=1;
	private $pageSize=20;
	private $numberRegister=[];
	private $searchTerm=null;
	public function setSearchTerm($searchTerm){
		$this->searchTerm=$searchTerm;
		return $this;
	}

	public function run(){
		$staffRepository = $this->model("StaffRepository");
		$startAt = ($this->page-1)*$this->pageSize;
		$staffMembers = $staffRepository->loadAll($startAt,$this->pageSize,$this->searchTerm);
		$totalCount = $staffRepository->countAll($this->searchTerm);
		$totalPages = ceil($totalCount/$this->pageSize);
		$lastPage = true;
		if($this->page < $totalPages)$lastPage = false;

		if($this->searchTerm){
			Console::print(0,0,'Search results [Search term: '.$this->searchTerm.'] ');
		}
		else{
			Console::print(0,0,'List of staff members ');
		}

		Console::print(0,0,'[Total: '.$totalCount.'] ');
		Console::print(0,0,'[Pages: '.$totalPages.'] ');
		Console::print(0,2,'[Current Page: '.$this->page.'] ');

		Console::printCell(0,0,'No',4);
		Console::printCell(0,0,'FIRST NAME');
		Console::printCell(0,0,'LAST NAME');
		Console::printCell(0,0,'EMAIL');
		Console::printCell(0,0,'PHONE');
		Console::printCell(0,0,'PHONE2');
		Console::printCell(0,1,'COMMENT',10);

		$this->numberRegister=[];
		$i=0;

		foreach($staffMembers as $staffMember){
			$i++;
			$this->numberRegister[$i]=$staffMember->getId();
			Console::printCell(0,0,$i,4);
			Console::printCell(0,0,$staffMember->getFirstname());
			Console::printCell(0,0,$staffMember->getLastname());
			Console::printCell(0,0,$staffMember->getEmail());
			Console::printCell(0,0,$staffMember->getPhonenumber1());
			Console::printCell(0,0,$staffMember->getPhonenumber2());
			Console::printCell(0,1,$staffMember->getComment(),10);
		}

		if($lastPage){
			Console::print(1,0,'"[e]xit" or [enter] or "[f]orward" to exit list; ');
		}
		else{
			Console::print(1,0,'"[e]xit" to exit list; ');
			Console::print(1,0,'[enter] or "[f]orward" for next page; ');
		}
		if($this->page!==1){
			Console::print(1,0,'"[b]ack" - previous page; ');
		}
		Console::print(1,0,'"[v]iew [number]" - View/Edit Staff Member.');

		$input = Console::read(1,1);
		$firstLetter = strtolower($input);
		if(strlen($firstLetter)>0)$firstLetter=$firstLetter[0];
		switch($firstLetter){
			case 'e':
				return true;
			case '':
			case 'f':
				if($lastPage){
					return true;
				}
				else{
					$this->page++;
					return $this->run();
				}
			case 'b':
				if($this->page > 1){
					$this->page--;
					return $this->run();
				}
				break;
			case 'v':
				$number = preg_replace('/[^0-9]/', '', $input);
				if(isset($this->numberRegister[$number])){
					return $this->viewStaffMember($this->numberRegister[$number]);
				}
				break;
		}
		return $this->run();
	}

	public function viewStaffMember($id){
		Console::print(1,1,'You are now viewing staff member:');
		$staffRepository = $this->model("StaffRepository");
		$staffMember = $staffRepository->loadById($id);
		Console::print(0,0,'Database ID:   ');
		Console::print(0,1,$staffMember->getId());
		Console::print(0,0,'FIRST NAME:    ');
		Console::print(0,1,$staffMember->getFirstname());
		Console::print(0,0,'LAST NAME:     ');
		Console::print(0,1,$staffMember->getLastname());
		Console::print(0,0,'EMAIL:         ');
		Console::print(0,1,$staffMember->getEmail());
		Console::print(0,0,'PHONE NUMBER:  ');
		Console::print(0,1,$staffMember->getPhonenumber1());
		Console::print(0,0,'PHONE NUMBER2: ');
		Console::print(0,1,$staffMember->getPhonenumber2());
		Console::print(0,0,'Comment:       ');
		Console::print(0,1,$staffMember->getComment());


		Console::print(1,0,'"[enter] to exit to list; ');
		Console::print(1,0,'"[e]dit" to edit members information; ');
		Console::print(1,0,'"[d]elete" - delete staff member. ');
		$input = Console::read(1,0);
		$firstLetter = strtolower($input);
		if(strlen($firstLetter)>0)$firstLetter=$firstLetter[0];
		switch($firstLetter){
			case '':
				return $this->run();
			case 'd':
				Console::print(1,0,'Are you sure you want to delete this staff member? <y/n> ');
				$input = Console::read(0,1);
				if($input=="y"){
					$staffRepository->deleteById($id);
					Console::print(0,1,'"Staff Member has been removed"');
					return $this->run();
				}
				return $this->viewStaffMember($id);
			case 'e':
				return $this->editStaffMember($id);
		}
		return $this->viewStaffMember($id);
	}

	public function editStaffMember($id){
		$staffRepository = $this->model("StaffRepository");
		$staffMember = $staffRepository->loadById($id);

		Console::print(0,1,'You are now editing staff member '.$staffMember->getFirstname().' '.$staffMember->getLastname());

		Console::print(0,0,'Database ID:');
		Console::print(0,1,$staffMember->getId());

		Console::print(0,0,'FIRST NAME: <'.$staffMember->getFirstname().'>');
		$input = Console::read(0,0);
		if($input!=="")$staffMember->setFirstname($input);

		Console::print(0,0,'LAST NAME: <'.$staffMember->getLastname().'> ');
		$input = Console::read(0,0);
		if($input!=="")$staffMember->setLastname($input);

		Console::print(0,0,'EMAIL: <'.$staffMember->getEmail().'> ');
		$input = Console::read(0,0);
		if($input!=="")$staffMember->setEmail($input);

		Console::print(0,0,'PHONE NUMBER: <'.$staffMember->getPhonenumber1().'> ');
		$input = Console::read(0,0);
		if($input!=="")$staffMember->setPhonenumber1($input);

		Console::print(0,0,'PHONE NUMBER2: <'.$staffMember->getPhonenumber2().'> ');
		$input = Console::read(0,0);
		if($input!=="")$staffMember->setPhonenumber2($input);

		Console::print(0,0,'Comment: <'.$staffMember->getComment().'> ');
		$input = Console::read(0,0);
		if($input!=="")$staffMember->setComment($input);

		if($staffMember->save()){
			Console::print(0,1,'Staff member have been saved successfully');
		}
		else{
			Console::print(0,1,'Staff member have not been saved');
		}
		return $this->viewStaffMember($id);

	}

	public function getDescription(){
		return 'Lists staff members.';
	}
}
