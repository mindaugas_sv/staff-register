<?php
namespace Mindaugas\StaffRegister\Functions\Staff;

use Mindaugas\StaffRegister\Core\CliFunction;
use Mindaugas\StaffRegister\Core\Console;

class Add extends CliFunction
{
	public function run()
	{
		Console::print(0,1,'You are now adding new staff member ');
		$newStaffMember = $this->model('StaffMember');
		Console::print(0,0,'First Name: ');
		$newStaffMember->setFirstname(Console::read(0,0));
		Console::print(0,0,'Last Name: ');
		$newStaffMember->setLastname(Console::read(0,0));
		Console::print(0,0,'Email: ');
		$newStaffMember->setEmail(Console::read(0,0));
		Console::print(0,0,'Phone Number: ');
		$newStaffMember->setPhonenumber1(Console::read(0,0));
		Console::print(0,0,'Second Phone Number: ');
		$newStaffMember->setPhonenumber2(Console::read(0,0));
		Console::print(0,0,'Coment: ');
		$newStaffMember->setComment(Console::read(0,0));
		if($newStaffMember->save()){
			Console::print(0,1,'New staff member added.');
		}
		else{
			Console::print(0,1,'Adding new staff member failed...');
		}
	}

	public function getDescription()
	{
		return 'Opens dialog for adding new staff member';
	}
}
