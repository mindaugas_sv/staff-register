# README #

### Functionality implemented ###

* Help
* Staff list
* Register a new person
* Delete a person from the register
* Find a person in the register
* Import of persons using a CSV-file

### How do I get set up? ###

* navigate to directory
* run "composer install"
* setup database (database.sql file included in root directory)
* test csv import (staffsample.csv file included in rootdirectory)
* phpunit is required in composer, for tests you can run "vendor\bin\phpunit test"
* there may be some errors regarding file names, please change file names if necessary (I worked on windows, app hasn't been tested on other platforms)

### What wasn’t implemented? ###

* Validation
* Config options to setup environment (for example: choose database login info, choose console width, staff quantity per page.)
* Setup (application could have had ability to create table in database)
* More options for information storage (saving in file, nosql databases)
* Command promt screen crearing 
* Views 
* ORM, I actualy tried to setup doctrine, but failed with config :) (took a lot of time)
* full test coveridge
* user optional input model (now there are a lot of similar code in functions)
* ability to import from another file types
* Both StaffMember and StaffRepository has DB queries, it would be nice to have seperate resourceModel
* StaffRepository now returns an array of StaffMember objects, it would be nice to return Object of StaffCollection (StaffCollection does not exist now)
* and mutch much more is missing...
