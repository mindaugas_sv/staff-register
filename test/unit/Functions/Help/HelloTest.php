<?php
use PHPUnit\Framework\TestCase;
use Mindaugas\StaffRegister\Functions\Help\Hello;

class HelloTest extends TestCase {
	public function testReturnsDescription(){
		$hello = new Hello;
		$this->assertTrue(is_string($hello->getDescription()));
	}
	public function testRunsSuccessfully(){
		$hello = new Hello;
		$this->assertTrue($hello->run());
	}
}
