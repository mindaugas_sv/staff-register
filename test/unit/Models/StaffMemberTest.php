<?php
use PHPUnit\Framework\TestCase;
use Mindaugas\StaffRegister\Models\StaffMember;

class StaffMemberTest extends TestCase
{
	public function setUp()
	{
		//$this->fetch = new StaffMember;
	}

	public function testStoresListOfAttributes()
	{
		$this->assertClassHasAttribute('id', StaffMember::class);
		$this->assertClassHasAttribute('firstname', StaffMember::class);
		$this->assertClassHasAttribute('lastname', StaffMember::class);
		$this->assertClassHasAttribute('email', StaffMember::class);
		$this->assertClassHasAttribute('phonenumber1', StaffMember::class);
		$this->assertClassHasAttribute('phonenumber2', StaffMember::class);
		$this->assertClassHasAttribute('comment', StaffMember::class);
	}
}
