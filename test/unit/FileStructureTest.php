<?php
use PHPUnit\Framework\TestCase;

class FileStructureTestt extends TestCase
{

	public function testFileStructure()
	{
		$this->assertDirectoryExists('src/Core');
		$this->assertDirectoryExists('src/Functions/Help');
		$this->assertDirectoryExists('src/Functions/Quit');
		$this->assertDirectoryExists('src/Functions/Staff');
		$this->assertDirectoryExists('src/Models');
	}
}
